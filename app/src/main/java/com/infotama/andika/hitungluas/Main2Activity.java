package com.infotama.andika.hitungluas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private EditText editPanjang, editLebar;
    private Button buttonHitung;
    private TextView textHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonHitung = (Button)findViewById(R.id.buttonHitung);
        editPanjang = (EditText)findViewById(R.id.editPanjang);
        editLebar = (EditText)findViewById(R.id.editLebar);
        textHasil = (TextView)findViewById(R.id.textHasil);

        buttonHitung.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                String panjang = editPanjang.getText().toString();
                String lebar = editLebar.getText().toString();

                if(editPanjang.length()!=0 && editLebar.length()!=0) {
                    double p = Double.parseDouble(panjang);
                    double l = Double.parseDouble(lebar);

                    double luas = p * l;

                    //DecimalFormat format = new DecimalFormat("#.##");

                    textHasil.setText("Luas" + luas);
                }
                else {
                    Toast.makeText(Main2Activity.this,"Kolom Panjang dan Lebar harus diisi (main2)",Toast.LENGTH_LONG).show();
                }
            }
        });


    }

}
